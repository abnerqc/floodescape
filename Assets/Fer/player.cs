﻿using UnityEngine;
using System.Collections;

public class player : MonoBehaviour {

	public float moveSpeed = 30f;
	private bool Walk;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void FixedUpdate ()
	{
		Move ();
	}

	public void Move ()
	{
		if (Walk) {
			Walk = false;
		}
		
		if (Input.GetKey (KeyCode.W) && Walk == false) 
		{
			Walk = true; 
			transform.Translate (-Vector2.down * moveSpeed * Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.S)) 
		{
			Walk = true;
			transform.Translate (Vector2.down * moveSpeed * Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.D)) 
		{
			Walk = true;
			transform.Translate (Vector2.right * moveSpeed * Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.A)) 
		{
			Walk = true;
			transform.Translate (Vector2.left * moveSpeed * Time.deltaTime);
		}
	}
}
