﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameAdmin : MonoBehaviour {

	GraphPlayer[] players;

	[HideInInspector]
	public GraphPlayer activePlayer;

	WaterBehavior water;

	Grafo[] graphs;

	public static GameAdmin instance;

	public Camera mainCamera;
	public Slider cameraSlider;


	// Use this for initialization
	void Start () {
		GameObject[] aux = GameObject.FindGameObjectsWithTag ("Player");
//		players = new GraphPlayer[aux.Length];
//		Debug.Log ("Number of players: " + aux.Length.ToString ());
//		for (int i = 0; i < aux.Length; i++) {
//			players [i] = aux[i].GetComponent<GraphPlayer> ();
//			Debug.Log (players[i].name);
//			players [i].Awake = false;
//		}
//
//		if (mainCamera == null)
//			mainCamera = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Camera>();
//		players [0].WakeUp ();
//		activePlayer = players[0];

		// Hay que asegurarse de que al finalizar el turno de un jugador se pase al siguiente.

		water = GameObject.FindGameObjectWithTag ("Water").GetComponent<WaterBehavior>();
//		cameraSlider.maxValue = mainCamera.GetComponent<CameraFollow> ().maxXAndY.y;
//		cameraSlider.minValue = mainCamera.GetComponent<CameraFollow> ().minXAndY.y;
	}
	
	// Update is called once per frame
//	void Update () {
//		if (cameraSlider.OnDrag) {
//			//Esto tiene que ir en el código del slider, no aquí
//			mainCamera.transform.position = new Vector2(mainCamera.transform.position.x, cameraSlider.value);
//		}
//	}

	void Turn(GraphPlayer player){
		player.WakeUp ();
		activePlayer = player;
	}

	void WaterTurn(){
		//Random Water behavior

		Turn (players [0]);
	}

	void TrackSliderPosition(){

	}

	void EndTurn(GraphPlayer player){
//		player.Sleep ();
		for (int i = 0; i < players.Length; i++) {
			if (players [i] == player) {
				if (i+1 < players.Length) {
					Turn (players [i + 1]);
					break;
				} else {
					WaterTurn ();
					Turn(players[0]);
				}
			}
		}
	}
}
