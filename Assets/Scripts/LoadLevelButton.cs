﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadLevelButton : MonoBehaviour {

	/// <summary>
	/// The level to be loaded when click this button.
	/// </summary>
	[Tooltip("The name of the scene to be loaded")]
	public string level;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void LoadLevel(){
		SceneManager.LoadScene (level);
	}
}
