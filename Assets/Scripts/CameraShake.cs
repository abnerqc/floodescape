﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {

	public bool shake = false;
	public float amplitud = 0.1f;
	public float duracion = 2.0f;
	private float reset;
	public Transform min;
	public Transform max;

	private Vector3 posicionInicial;

	// Use this for initialization
	void Start () {

		reset = duracion;
		posicionInicial = transform.localPosition;

	}
	
	// Update is called once per frame
	void Update () {

		if (shake == true) {
			interpolacionCamara ();
			shakeCamara ();
		}
	
	}

	public void interpolacionCamara(){
		{
			
			transform.position = new Vector3 (transform.position.x, Mathf.Lerp(min.position.y, max.position.y, .5f), transform.position.z);

		}

	}


	public void shakeCamara()
	{
		transform.localPosition = transform.localPosition + Random.insideUnitSphere * amplitud;
		duracion -= Time.deltaTime;

		if (duracion <= 0) {
			shake = false;
			duracion = reset;
			transform.localPosition = posicionInicial;
		}

	}

}
