using UnityEngine;
using System.Collections;

public class Game_Flex : MonoBehaviour {
	public string currentState;
	public FlexFSM fsm;
	public GameData_Flex gameData;

	void Start()
	{
		InitializeGameData();
		MakeFSM();
	}

	void InitializeGameData()
	{
		gameData = new GameData_Flex();
//		gameData.fatigue = 0;
//		gameData.goldInPockets = 0;
//		gameData.thirst = 0;
	}

	void MakeFSM()
	{
		fsm = new FlexFSM(gameObject,this);

//		Mining mining = new Mining(this);
//		Drinking drinking = new Drinking(this);
//		Sleeping sleeping = new Sleeping(this);
//		MakingBankDeposit makingBankDeposit = new MakingBankDeposit(this);
//
//		fsm.AddState(StateID.Mining, mining);
//		fsm.AddState(StateID.Drinking, drinking);
//		fsm.AddState(StateID.Sleeping, sleeping);
//		fsm.AddState(StateID.MakingBankDeposit, makingBankDeposit);
//
//		//fsm.ChangeState(StateID.Mining);
//		fsm.ChangeState(StateID.Sleeping);

		fsm.Activate();
	}

	void Update()
	{
		if (fsm != null && fsm.IsActive())
		{
			fsm.UpdateFSM();
			currentState = fsm.GetCurrentStateName();
		}
	}

	public void LogAction(string msg)
	{
		Debug.Log(System.DateTime.Now.ToString("HH:mm:ss") + " - " + msg);
	}
//	public bool IsFatigued()
//	{
//		if (gameData.fatigue >= 500) return true;
//		return false;
//	}
//	public bool HasPocketsFull()
//	{
//		if (gameData.goldInPockets >= 200) return true;
//		return false;
//	}
//	public bool IsThirsty()
//	{
//		if (gameData.thirst >= 600) return true;
//		return false;
//	}
}