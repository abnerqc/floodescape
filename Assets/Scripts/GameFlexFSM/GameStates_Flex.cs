﻿using UnityEngine;
using System.Collections;

namespace GameStates
{
	public enum StateID
	{
		GameStart,
		PlayersItemsTurns,
		PlayersTurns,
		WaterTurn,
		WaterRandomTurn,
		EndGame


	}

	//==================GameStart=========================
	//====================================================
	public class GameStart : FlexState
	{
		private Game_Flex game;
		private GameData_Flex gameData;

		public GameStart(Game_Flex _game)
		{
			game = _game;
			gameData = game.gameData;
		}

		public override void OnEnter(GameObject owner)
		{
//			game.LogAction("Entering mine");
		}

		public override void OnExit(GameObject owner)
		{
//			game.LogAction("Leaving mine with " + gameData.goldInPockets + " gold in my pockets");
		}

		public override void Reason(GameObject owner)
		{
//			if (game.IsFatigued()) ChangeState(StateID.Sleeping);
//			if (game.HasPocketsFull()) ChangeState(StateID.MakingBankDeposit);
//			if (game.IsThirsty()) ChangeState(StateID.Drinking);
		}

		public override void Act(GameObject owner)
		{
//			gameData.fatigue += 5;
//			gameData.goldInPockets += 1;
//			gameData.thirst += 3;
			//miner.LogAction("Current gold: " + minerData.goldInPockets);
		}
	}

	//==================PlayersItemsTurn==================
	//====================================================
	public class PlayersItemsTurn : FlexState
	{
		private Game_Flex game;
		private GameData_Flex gameData;

		public PlayersItemsTurn(Game_Flex _game)
		{
			game = _game;
			gameData = game.gameData;
		}

		public override void OnEnter(GameObject owner)
		{
//			game.LogAction("Entering mine");
		}

		public override void OnExit(GameObject owner)
		{
			//			game.LogAction("Leaving mine with " + gameData.goldInPockets + " gold in my pockets");
		}

		public override void Reason(GameObject owner)
		{
			//			if (game.IsFatigued()) ChangeState(StateID.Sleeping);
			//			if (game.HasPocketsFull()) ChangeState(StateID.MakingBankDeposit);
			//			if (game.IsThirsty()) ChangeState(StateID.Drinking);
		}

		public override void Act(GameObject owner)
		{
			//			gameData.fatigue += 5;
			//			gameData.goldInPockets += 1;
			//			gameData.thirst += 3;
			//miner.LogAction("Current gold: " + minerData.goldInPockets);
		}
	}

	//==================PlayersTurn=======================
	//====================================================
	public class PlayersTurn : FlexState
	{
		private Game_Flex game;
		private GameData_Flex gameData;

		public PlayersTurn(Game_Flex _game)
		{
			game = _game;
			gameData = game.gameData;
		}

		public override void OnEnter(GameObject owner)
		{
//			game.LogAction("Entering mine");
		}

		public override void OnExit(GameObject owner)
		{
			//			game.LogAction("Leaving mine with " + gameData.goldInPockets + " gold in my pockets");
		}

		public override void Reason(GameObject owner)
		{
			//			if (game.IsFatigued()) ChangeState(StateID.Sleeping);
			//			if (game.HasPocketsFull()) ChangeState(StateID.MakingBankDeposit);
			//			if (game.IsThirsty()) ChangeState(StateID.Drinking);
		}

		public override void Act(GameObject owner)
		{
			//			gameData.fatigue += 5;
			//			gameData.goldInPockets += 1;
			//			gameData.thirst += 3;
			//miner.LogAction("Current gold: " + minerData.goldInPockets);
		}
	}

	//==================WaterTurn=========================
	//====================================================
	public class WaterTurn : FlexState
	{
		private Game_Flex game;
		private GameData_Flex gameData;

		public WaterTurn(Game_Flex _game)
		{
			game = _game;
			gameData = game.gameData;
		}

		public override void OnEnter(GameObject owner)
		{
//			game.LogAction("Entering mine");
		}

		public override void OnExit(GameObject owner)
		{
			//			game.LogAction("Leaving mine with " + gameData.goldInPockets + " gold in my pockets");
		}

		public override void Reason(GameObject owner)
		{
			//			if (game.IsFatigued()) ChangeState(StateID.Sleeping);
			//			if (game.HasPocketsFull()) ChangeState(StateID.MakingBankDeposit);
			//			if (game.IsThirsty()) ChangeState(StateID.Drinking);
		}

		public override void Act(GameObject owner)
		{
			//			gameData.fatigue += 5;
			//			gameData.goldInPockets += 1;
			//			gameData.thirst += 3;
			//miner.LogAction("Current gold: " + minerData.goldInPockets);
		}
	}

	//==================WaterRandomTurn===================
	//====================================================
	public class WaterRandomTurn : FlexState
	{
		private Game_Flex game;
		private GameData_Flex gameData;

		public WaterRandomTurn(Game_Flex _game)
		{
			game = _game;
			gameData = game.gameData;
		}

		public override void OnEnter(GameObject owner)
		{
//			game.LogAction("Entering mine");
		}

		public override void OnExit(GameObject owner)
		{
			//			game.LogAction("Leaving mine with " + gameData.goldInPockets + " gold in my pockets");
		}

		public override void Reason(GameObject owner)
		{
			//			if (game.IsFatigued()) ChangeState(StateID.Sleeping);
			//			if (game.HasPocketsFull()) ChangeState(StateID.MakingBankDeposit);
			//			if (game.IsThirsty()) ChangeState(StateID.Drinking);
		}

		public override void Act(GameObject owner)
		{
			//			gameData.fatigue += 5;
			//			gameData.goldInPockets += 1;
			//			gameData.thirst += 3;
			//miner.LogAction("Current gold: " + minerData.goldInPockets);
		}
	}

	//==================EndGame===========================
	//====================================================
	public class EndGame : FlexState
	{
		private Game_Flex game;
		private GameData_Flex gameData;

		public EndGame(Game_Flex _game)
		{
			game = _game;
			gameData = game.gameData;
		}

		public override void OnEnter(GameObject owner)
		{
//			game.LogAction("Entering mine");
		}

		public override void OnExit(GameObject owner)
		{
			//			game.LogAction("Leaving mine with " + gameData.goldInPockets + " gold in my pockets");
		}

		public override void Reason(GameObject owner)
		{
			//			if (game.IsFatigued()) ChangeState(StateID.Sleeping);
			//			if (game.HasPocketsFull()) ChangeState(StateID.MakingBankDeposit);
			//			if (game.IsThirsty()) ChangeState(StateID.Drinking);
		}

		public override void Act(GameObject owner)
		{
			//			gameData.fatigue += 5;
			//			gameData.goldInPockets += 1;
			//			gameData.thirst += 3;
			//miner.LogAction("Current gold: " + minerData.goldInPockets);
		}
	}
}
