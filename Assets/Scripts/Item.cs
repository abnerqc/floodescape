﻿using UnityEngine;
using System.Collections;

public enum Items{ Zombi, Miel, Brea, Abejas, Bota, TalismanAzul, TalismanVerde, Muerte}

[ExecuteInEditMode]
public class Item : MonoBehaviour {

	public Items item;

	public Sprite Honey;
	public Sprite Bee;
	public Sprite Bomb;
	public Sprite Boot;
	public Sprite BlueTalisman;
	public Sprite GreenTalisman;
	public Sprite Skull;
	public Sprite Zombie;

	bool activateOnSelf = true;

//	SpriteRenderer renderer = GetComponent<SpriteRenderer> ();

	// Use this for initialization
	void Awake () {
//		renderer = GetComponent<SpriteRenderer> ();
		int it = Random.Range(1, 8);

		switch (it) {
		case 1:	//Honey
			item = Items.Miel;
			break;

		case 2:	//Bree
			item = Items.Abejas;
			break;
		case 3:	//bomb
			item = Items.Brea;
			break;
		case 4:	//Boot
			item = Items.Bota;
			break;
		case 5:	//Blue Talisman
			item = Items.TalismanAzul;
			break;
		case 6:	//Green Talisman
			item = Items.TalismanVerde;
			break;
		case 7:	//Skull
			item = Items.Muerte;
			break;
		case 8:	//Zombie
			item = Items.Zombi;
			break;

		}
	}
	
	// Update is called once per frame
	void Update () {
		switch (item) {
		case Items.Abejas:
			GetComponent<SpriteRenderer>().sprite = Bee;
			break;
		case Items.Bota:
			GetComponent<SpriteRenderer>().sprite = Boot;
			break;
		case Items.Brea:
			GetComponent<SpriteRenderer>().sprite = Bomb;
			break;
		case Items.Miel:
			GetComponent<SpriteRenderer>().sprite = Honey;
			break;
		case Items.Muerte:
			GetComponent<SpriteRenderer>().sprite = Skull;
			break;
		case Items.TalismanAzul:
			GetComponent<SpriteRenderer>().sprite = BlueTalisman;
			break;
		case Items.TalismanVerde:
			GetComponent<SpriteRenderer>().sprite = GreenTalisman;
			break;
		case Items.Zombi:
			GetComponent<SpriteRenderer>().sprite = Zombie;
			break;
		}
	}

	void Activate(){
		
	}

	public void Activate(GraphPlayer player){
		Debug.Log ("Activate");

		if (item == Items.Abejas) {
			player.moving = true;
			player.spacesToMove++;
			Debug.Log ("Abejas, se mueve un espacio extra:" + player.spacesToMove.ToString());
		}
		if (item == Items.Miel) {
			player.stamina += 1;
			Debug.Log ("Miel, aumenta stamina:" + player.stamina.ToString());
		}
		if (item == Items.Muerte) {
			//Bajar casillas.
			player.Fall (1);
		}
		if (item == Items.TalismanAzul) {
			//Inmunidad contra el agua
		}
		if (item == Items.TalismanVerde) {
			//CAmbia de posición con otro jugador
		}
		if (item == Items.Zombi) {
			player.stamina -= 1;
		}
		if (item == Items.Bota) {
			player.Fall (1);
			Debug.Log ("Activate Boot");
		}
		if (item == Items.Brea) {
			player.stamina -= 1;
			Debug.Log ("Activate Brea");
		}
	}

	public bool activeOnSelf(){
		if (item == Items.Bota || item == Items.Brea) {
			return false;
		} else
			return true;
	}
}