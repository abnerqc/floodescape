﻿using UnityEngine;
using System.Collections;

public enum PlayerSlot { PlayerOne, PlayerTwo, PlayerThree, PlayerFour };
public class ChooseTargetButtonScript : MonoBehaviour {

	public PlayerSlot playerSlot;
	public GameAdmin admin;
	public GraphPlayer ThisPlayer;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ChooseThis(){
		admin.activePlayer.SendMessage ("ChooseTargetPlayer", ThisPlayer);
	}
}
