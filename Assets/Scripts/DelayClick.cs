﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum Tiros {Uno, Dos, Tres};

public class DelayClick : MonoBehaviour {
	[Tooltip("Time for slider to go up 1 point.")]
	[Range(1,3)]
	public float timeToRise;

	GraphPlayer player;
	GameAdmin admin;

//	[Tooltip("If button is pressed more than this time, you use 3 stamina")]
//	[Range(2,6)]
//	public float timeToThree;

	[Tooltip("Slider (UI element) used to track the strength movement")]
	public Slider bar;

	[Tooltip("Used to track players stamina in the UI")]
	public Text staminaText;

	[Tooltip("Used to track max throw in UI's slider")]
	public Text maxThrowText;

	[Tooltip("Used to track current throw in UI's slider")]
	public Text throwText;
		
//	public Juego juego;
	
	//Time between click and now
	float totalTime;
	//Time when button is clicked
	float clickTime;
	//Used to track time
	bool counting = false;

	int maxThrow = 3;


	bool up = true;

	Tiros tiro;

	void Start(){
		bar.minValue = 0;
		bar.maxValue = maxThrow + 0.5f;
		throwText.text = "0";
		maxThrowText.text = "3";
		admin = GameObject.Find ("Admin").GetComponent<GameAdmin> ();
	}

	void Update(){
		player = admin.activePlayer;
		bar.maxValue = (int)player.stamina;

		if (bar.maxValue > maxThrow) {
			bar.maxValue = maxThrow;
		}

		maxThrowText.text = ((int)(player.stamina)).ToString();
//		throwText.text = "0";

		if (counting) {
			
			totalTime = Time.time - clickTime;
			if (up) {
				bar.value = totalTime;
				if(bar.value > 0 && bar.value < 1)
					throwText.text = "1";
				if(bar.value > 1 && bar.value < 2)
					throwText.text = "2";
				if(bar.value > 2 && bar.value < 3)
					throwText.text = "3";
				
				if (bar.value >= bar.maxValue) {
					clickTime = Time.time;
					up = false;
				}
			}
			else {
				bar.value = bar.maxValue - totalTime;

				if(bar.value > 0 && bar.value < 1)
					throwText.text = "1";
				if(bar.value > 1 && bar.value < 2)
					throwText.text = "2";
				if(bar.value > 2 && bar.value < 3)
					throwText.text = "3";
				if (bar.value <= 0) {
					clickTime = Time.time;
					up = true;
				}
			}

		}

		float aux = player.stamina;

		if (aux > 0 && aux < 1) {
			maxThrowText.text = "0";
		}
		if (aux > 1 && aux < 2) {
			maxThrowText.text = "1";
		}
		if (aux > 2 && aux < 3) {
			maxThrowText.text = "2";
		}
		if (aux >= 3) {
			maxThrowText.text = "3";
		}


//		maxThrowText.text = ((int)(player.stamina + player.staminaRegain)).ToString();
	}

	public void OnMouseDown(){
		//We keep the time and start counting
		if (!player.moving) {
			clickTime = Time.time;
			counting = true;
			up = true;
		}
//		Debug.Log ("Click at: " + clickTime);
	}
	
	public void OnMouseUp(){
		//We select the throw and then we stop counting
		if (!player.moving) {
//			float aux = (bar.value / timeToRise);
			float aux = bar.value;
			int tiro = 0;
			if(aux > 0 && aux < 1)
				tiro = 1;
			if(aux > 1 && aux < 2)
				tiro = 2;
			if(aux > 2 /*&& aux < 3*/)
				tiro = 3;
			
			if (player.stamina >= tiro) {
				player.Move (tiro);
				player.stamina -= tiro;
				if (player.stamina + player.staminaRegain < 2) {
					bar.maxValue = 1;
					maxThrowText.text = "1";
					throwText.text = "0";

				} else {
					bar.maxValue = (int)player.stamina + player.staminaRegain;
					if (bar.maxValue > maxThrow) {
						bar.maxValue = maxThrow;
					}
					maxThrowText.text = ((int)(player.stamina + player.staminaRegain)).ToString();
					throwText.text = "0";
				}
//				bar.maxValue = (player.stamina < 2 ? 1 : (int)player.stamina + 1);
			} else {
				Debug.Log ("No hay suficiente energía");
			}

			counting = false;
			totalTime = 0;
			bar.value = totalTime;

		}
	}
	
	void Tirar(){
//		juego.RecibirTiro(tiro);
	}
}
