﻿public class CharacterData_Flex
{

}

/// <summary>
/// Eventos que pueden escuchar elsa y bob
/// </summary>

public abstract class Events
{
	
	public static string v_move = "v_move";
	public static string d_move = "d_move";
	public static string direction_change = "direction_change";
	public static string stop_move = "stop_move";
	public static string win = "win";
	public static string die = "die";
	public static string lose = "lose";
	public static string bonus = "bonus";
	public static string damage = "damage";
	public static string d_idle = "d_idle";
	public static string v_idle = "v_idle";

}