﻿using UnityEngine;
using System.Collections;

public class scaleWater : MonoBehaviour {

	public bool waterActive = false;
	float duracion;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
		duracion = Random.Range (0f, 0.5f);

		
		if (waterActive == true) {
			water();
		}
		
	}

	void water (){

		transform.localPosition += new Vector3(0, Random.Range(0f, 1f), 0);
		duracion -= Time.deltaTime;

		if (duracion <= 0) {
			waterActive = false;

			duracion = 0;
		}
	}


}