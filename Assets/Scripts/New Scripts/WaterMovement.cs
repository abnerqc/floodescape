﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterMovement : MonoBehaviour {

	public Vector2 speed;
	public GameObject player;
	bool avanzar = false;
	public float velocidadMax;
	int aux = 10;


	IEnumerator inicio() {
		
		yield return new WaitForSeconds (4f);
		avanzar = true;

	}

	// Use this for initialization
	void Start () {

		player.transform.position = GameObject.Find("Player").transform.position;
		StartCoroutine(inicio());
		
	}
	
	// Update is called once per frame
	void Update () {

		if (avanzar)
		{
			transform.position += (Vector3)speed;
			velocidad ();
		}	
	}

	void velocidad()
	{
		if (player.transform.position.y >= aux) 
		{
			
			speed += new Vector2 (0, 0.01f);
			transform.position += (Vector3)speed;
			aux += 10;

			if (speed.y >= velocidadMax)
				speed = new Vector2 (0, 0.04f);
		}


	}

}