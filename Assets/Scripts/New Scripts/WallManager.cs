﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallManager : MonoBehaviour 
{
	public Transform ref_player;
	public GameObject[] sectionPrefabs;
	public float spawnY = 3.0f;
	public float MLength = 3.0f;

	public int poolSize = 2;
	public float createTime = 0.5f;
	private List <GameObject> activeSegment;

	private int lastSegmentIndex = 0;
	private bool willGrow = false;	
	public static WallManager poolGeneric;

	void Awake()
	{
		poolGeneric = this;
	}

	// Use this for initialization
	void Start () 
	{
		
		InvokeRepeating("SegmentLoop", createTime, createTime);
		ref_player = GameObject.FindGameObjectWithTag ("Player").transform;
		activeSegment = new List<GameObject> ();
		for (int i = 0; i < poolSize; i++) 
		{
			if (i < 2)
				MapRecycle (0);
			else
				MapRecycle ();
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		if (ref_player.position.y > (spawnY - poolSize * MLength)) 
		{
			MapRecycle ();
		}

	}

	private void MapRecycle (int prefabIndex = -1)
	{
		GameObject TrackSegment;
			if(prefabIndex == -1)
				TrackSegment = Instantiate (sectionPrefabs [RandomSegmentIndex ()]) as GameObject;
			else
				TrackSegment = Instantiate (sectionPrefabs [prefabIndex]) as GameObject;
				TrackSegment.transform.SetParent (transform);
		//TrackSegment.transform.position = Vector3.up * spawnY;
		//spawnY += MLength;
		TrackSegment.SetActive (false);
		activeSegment.Add (TrackSegment);
	}
		
	private int RandomSegmentIndex ()
	{
		if(sectionPrefabs.Length <= 1)
			return 0;

		int randomIndex = lastSegmentIndex;
		while (randomIndex == lastSegmentIndex) 
		{
			randomIndex = Random.Range (0, sectionPrefabs.Length);
		}

		lastSegmentIndex = randomIndex;
		return randomIndex;
	}

	public GameObject GetSegment ()
	{
		for(int i = 0; i < activeSegment.Count; i++)
			if( !activeSegment[i].activeInHierarchy)
			{
				// Si encontre un objeto disponible, lo regreso
				return activeSegment[i];
			}

		// el pool puede crecer
		if (willGrow)
		{
			// si puede crecer, creamos un nuevo objeto y lo añadimos al pool
			GameObject newObj = Instantiate(sectionPrefabs [RandomSegmentIndex ()]);
			newObj.SetActive(true);

			activeSegment.Add(newObj);

			return newObj;
		}

		// si no encontre nada disponible
		return null;
	}

	void SegmentLoop()
	{
		GameObject newObject = GetSegment();

		if( newObject == null) return; // no encontro objeto disponible, no pudo crear

		//se crea uno nuevo y se activa 
		newObject.transform.position =  new Vector3(Random.Range(-1, 1) * 5, spawnY * 5.0f, -0.1f);
		spawnY += MLength;
		newObject.SetActive(true);
	}
}