﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour {

	public Transform  respawnPosition;

	public string label;
	public GameObject[] pool;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/// <summary>
	/// Respawn the specified obj. in the pool
	/// </summary>
	/// <param name="obj">Object.</param>
	void Respawn(GameObject obj){
		//Turn off 
		obj.SetActive(false);
		//Change position
		obj.transform.position = respawnPosition.position;
		//Activate
		obj.SetActive(true);
	}
}
