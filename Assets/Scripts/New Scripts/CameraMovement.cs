﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

	public Vector2 speed;

	bool avanzar = false;


	IEnumerator inicio()
	{
		yield return new WaitForSeconds (4f);

		avanzar = true;
	}
	// Use this for initialization
	void Start () {

		StartCoroutine(inicio());
		
	}
	
	// Update is called once per frame
	void Update () {

		if(avanzar)
		transform.position += (Vector3)speed;
	}
}
