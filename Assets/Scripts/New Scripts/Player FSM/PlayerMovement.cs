﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using PlayerStates;

public class PlayerMovement : MonoBehaviour {

	public string currentState;
	public FlexFSM fsm;
	public PlayerData_Flex playerData;


	// Use this for initialization
	void Start () {
		InitializePlayerData ();
		MakeFSM ();
	}

	void InitializePlayerData(){
//		playerData = new PlayerData_Flex ();
		//Inicializar valores
	}

	void MakeFSM(){
		fsm = new FlexFSM (gameObject, this);

		//Estados
		PlayerStart playerStart = new PlayerStart(this);
		VerticalIdle verticalIdle = new VerticalIdle (this);
		FallInWater fallInWater = new FallInWater (this);
		GetPowerUp getPowerUp = new GetPowerUp (this);
		Stop stop = new Stop (this);

		//Añadis estados a la fsm
		fsm.AddState(StateID.PlayerStart, playerStart);
		fsm.AddState (StateID.VerticalIdle, verticalIdle);
		fsm.AddState (StateID.FallInWater, fallInWater);
		fsm.AddState (StateID.GetPowerUp, getPowerUp);
		fsm.AddState (StateID.Stop, stop);

		//fsm.ChangeState (StateID.VerticalIdle);
		fsm.ChangeState (StateID.PlayerStart);

		fsm.Activate ();
	}
	
	// Update is called once per frame
	void Update () {
		//if the player isn't stopped, it moves
//		if(!stop)
//			transform.position += (Vector3)speed;

		if (fsm != null && fsm.IsActive())
		{
			fsm.UpdateFSM();
			currentState = fsm.GetCurrentStateName();
			//checking = minerData.goldInPockets;
		}
	}


	/// <summary>
	/// Switchs the player's direction.
	/// </summary>
	void SwitchDirection(){

		//If this player is on the right side
		if (playerData.right) {
			//Moves the player to the other side
			transform.position = new Vector3 (transform.position.x + playerData.distanceToSide,
											transform.position.y, transform.position.z);

			playerData.Salto.Play ();
												
		} else {
			//Moves the player to the other side
			transform.position = new Vector3 (transform.position.x - playerData.distanceToSide,
											transform.position.y, transform.position.z);

			playerData.Salto.Play ();
		}

		//Changes the bool to track in wich side they are
		playerData.right = !playerData.right;

		//Changes the localScale of the player's GameObject so it will face the other side
		transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
	}

	public void LogAction(string msg)
	{
		Debug.Log(System.DateTime.Now.ToString("HH:mm:ss") + " - " + msg);
	}
	public void LogAction(string msg,string color)
	{
		Debug.Log(System.DateTime.Now.ToString("HH:mm:ss") + " - <color="+color+">" + msg+ "</color>");
	}
}