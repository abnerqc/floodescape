﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData_Flex : MonoBehaviour {

	/// <summary>
	/// Vector that a player uses to move
	/// </summary>
	public Vector2 speed;

	/// <summary>
	/// True if player is on the right side, false if it's on the left side.
	/// </summary>
	public bool right;

	/// <summary>
	/// The distance to switch the gameobject from side to side
	/// </summary>
	public float distanceToSide;

	/// <summary>
	/// The game admin.
	/// </summary>
	public NewAdmin admin;

	/// <summary>
	/// Used to track if this player has to stop (caused by a collision with an obstacle
	/// </summary>
	public bool stop;

	/// <summary>
	/// The seconds a player stops when hit.
	/// </summary>
	public float secondsStop = 2f;

	/// <summary>
	/// The power up sprite to show a player gets it.
	/// </summary>
	public GameObject PowerUp;


	public AudioSource Salto;
}
