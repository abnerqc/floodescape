﻿using UnityEngine;
using System.Collections;

namespace PlayerStates{

	public enum StateID{
		PlayerStart,
		VerticalIdle,
		FallInWater,
		GetPowerUp,
		Stop
	}


	public class PlayerStart : FlexState
	{

		private PlayerMovement player;
		private PlayerData_Flex playerData;
		public float presentAnim;

		public PlayerStart (PlayerMovement _player){
			player = _player;
			playerData = player.playerData;
		}

		public override void OnEnter (GameObject owner)
		{
			//Animación de entrada, inicia juego
			presentAnim = Time.time;
			setAnimation ("presentation", false);
		}

		public override void OnExit(GameObject owner){
			//Pasar a estado de avanzar?
		}

		public override void Reason (GameObject owner)
		{
			//Cuando acabe la animación? Pasar a estado avanzar
		} 

		public override void Act (GameObject owner)
		{
			
			if(Time.time - playerData.secondsStop >= presentAnim)
			ChangeState (StateID.VerticalIdle); 
			
		}
	}

	public class VerticalIdle : FlexState
	{

		private PlayerMovement player;
		private PlayerData_Flex playerData;

		public VerticalIdle (PlayerMovement _player){
			player = _player;
			playerData = player.playerData;
		}

		public override void OnEnter (GameObject owner)
		{
			//Animación de entrada, inicia juego
			Debug.Log("Vertical");
			playerData.Salto.Play ();
			setAnimation ("V_move", true);
		}

		public override void OnExit(GameObject owner){
			//Pasar a estado de avanzar?
		}

		public override void Reason (GameObject owner)
		{
			//			if(player.switchSide()){ fsm.PushBlipState( PlayerStates.SwitchSide);}

		} 

		public override void Act (GameObject owner)
		{
			
			player.transform.position += (Vector3)playerData.speed;

		}
	}

	public class FallInWater : FlexState
	{

		private PlayerMovement player;
		private PlayerData_Flex playerData;

		public FallInWater (PlayerMovement _player){
			player = _player;
			playerData = player.playerData;
		}

		public override void OnEnter (GameObject owner)
		{
			//Animación de entrada, inicia juego
			setAnimation ("V_die", true);
		}

		public override void OnExit(GameObject owner){
			//Pasar a estado de avanzar?
		}

		public override void Reason (GameObject owner)
		{
			//			if(player.switchSide()){ fsm.PushBlipState( PlayerStates.SwitchSide);}

		} 

		public override void Act (GameObject owner)
		{
			playerData.admin.SendMessage ("FellInWater", player);

		}
	}
		

	public class GetPowerUp : FlexState
	{

		private PlayerMovement player;
		private PlayerData_Flex playerData;

		public GetPowerUp (PlayerMovement _player){
			player = _player;
			playerData = player.playerData;
		}

		public override void OnEnter (GameObject owner)
		{
			//Animación de entrada, inicia juego

			setAnimation ("V_bonus", true);
		}

		public override void OnExit(GameObject owner){
			//Pasar a estado de avanzar?
		}

		public override void Reason (GameObject owner)
		{
			

		} 

		public override void Act (GameObject owner)
		{
			playerData.PowerUp.SetActive (true);

		}
	}


	public class Stop : FlexState
	{

		private PlayerMovement player;
		private PlayerData_Flex playerData;

		private float enterTime;

		public Stop (PlayerMovement _player){
			player = _player;
			playerData = player.playerData;
		}

		public override void OnEnter (GameObject owner)
		{
			//Animación de entrada, inicia juego
			enterTime = Time.time;
			setAnimation ("V_hurt", true);
		}

		public override void OnExit(GameObject owner){
			//Pasar a estado de avanzar?
		}

		public override void Reason (GameObject owner)
		{
			//			if(player.switchSide()){ fsm.PushBlipState( PlayerStates.SwitchSide);}
			if(Time.time - playerData.secondsStop >= enterTime) RevertBlipState();

		} 

		public override void Act (GameObject owner)
		{
			playerData.stop = true;

		}
	}

}