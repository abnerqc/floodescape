using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchButton : MonoBehaviour {

	public PlayerMovement player;

	public void Switch(){
		player.SendMessage ("SwitchDirection");
	}

	public void Stop(){
		player.SendMessage ("Fall");
	}
}
