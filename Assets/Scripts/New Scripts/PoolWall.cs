﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolWall : MonoBehaviour {

	static Dictionary<int, Queue<GameObject>> pool = new Dictionary<int,Queue <GameObject>> ();
	public float tiempo;
	public float tiempoDeVida;
	public float velocidad;
	public int total;
	public GameObject prefab;

	// Use this for initialization
	void Start () 
	{
		PreInstancia (prefab, total); 
		InvokeRepeating ("instanciar", 0, tiempo);
		Invoke ("Destruir", tiempoDeVida);
	}

	void Update ()
	{
		transform.position += Vector3.down * velocidad * Time.deltaTime; 
	}
	

	void instanciar () 
	{
		Reutilizar (prefab, transform.position);
	}

	void Destruir ()
	{
		gameObject.SetActive (false);
	}

	public static void PreInstancia (GameObject objetoAInstanciar, int Cantidad)
	{
		pool.Add(objetoAInstanciar.GetInstanceID(), new Queue<GameObject> ());

		for(int i = 0 ; i < Cantidad; i++)
		{
			GameObject go = Instantiate (objetoAInstanciar) as GameObject;
			pool [objetoAInstanciar.GetInstanceID ()].Enqueue (go);
			go.SetActive (false);

		}
	}

	public static void Reutilizar(GameObject objetoAInstanciar, Vector3 posicion)
	{
		if (pool.ContainsKey (objetoAInstanciar.GetInstanceID ())) 
		{
			GameObject go = pool [objetoAInstanciar.GetInstanceID ()].Dequeue ();
			//go.transform.position = posicion;
			go.transform.position = posicion += new Vector3(Random.Range (-1, 1) * 4, 0, -1);
			go.SetActive (true);
			pool [objetoAInstanciar.GetInstanceID ()].Enqueue (go);
		}
	}

}
