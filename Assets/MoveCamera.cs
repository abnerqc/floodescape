﻿using UnityEngine;
using System.Collections;

public class MoveCamera : MonoBehaviour {

	// The ID of the touch that began the scroll.
	int ScrollTouchID = -1;
	public bool scrolling = false;
	// Posicion inicial del touch
	Vector2 ScrollTouchOrigin;

	void Update()
	{
		foreach(Touch T in Input.touches)
		{
			//Note down the touch ID and position when the touch begins...
			if (T.phase == TouchPhase.Began)
			{                                    
				if (ScrollTouchID == -1)
				{
					ScrollTouchID = T.fingerId;    
					ScrollTouchOrigin = T.position;    
				}
			}
			//Forget it when the touch ends
			if ((T.phase == TouchPhase.Ended) || (T.phase == TouchPhase.Canceled))
			{                                    
				ScrollTouchID = -1;    
				scrolling = false;
			}
			if (T.phase == TouchPhase.Moved)
			{
				//If the finger has moved and it's the finger that started the touch, move the camera along the Y axis.
				if (T.fingerId == ScrollTouchID) {
					scrolling = true;
					Vector3 CameraPos = Camera.main.transform.position;
					Camera.main.transform.position = new Vector3 (CameraPos.x, CameraPos.y + T.deltaPosition.y, CameraPos.z);
				} 
			}
		}
	}
}
