﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionTwo : MonoBehaviour {

	private GameObject[] playerListTwo;
	private int indexTwo;

	// Use this for initialization
	void Start () {
		CharacterTwo ();
	}

	public void CharacterTwo ()
	{
		indexTwo = PlayerPrefs.GetInt ("playerTwo");

		playerListTwo = new GameObject[transform.childCount];

		// llena el arreglo con los personajes jugables
		for (int i = 0; i < transform.childCount; i++)
			playerListTwo [i] = transform.GetChild (i).gameObject;

		//activa el primer personaje jugable en el indice de la lista
		foreach (GameObject go in playerListTwo)
			go.SetActive (false);

		if (playerListTwo [indexTwo])
			playerListTwo [indexTwo].SetActive (true);
	}

	public void SelectCharacterTwoButton ()
	{
		playerListTwo[indexTwo].SetActive (false);

		indexTwo++;
		if (indexTwo == playerListTwo.Length)
			indexTwo = 0;

		playerListTwo [indexTwo].SetActive (true);
	}


	public void PlayGameButton ()
	{
		PlayerPrefs.SetInt ("playerTwo", indexTwo);
	}

	// Update is called once per frame
	void Update () {
		
	}
}
