﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public AudioClip musicThemeUI;
	public AudioClip Cenote;
	public AudioClip Fabrica;
	public AudioClip Magica;
	public AudioClip Pantano;
	public AudioClip Volcanica;
	private AudioSource Audio;

	public static SoundManager instance = null;

	void Awake ()
	{
		if (instance == null) 
			
			instance = this;
		
		else if (instance != this) 
		
			Destroy (gameObject);

		DontDestroyOnLoad (gameObject);

		Audio = GetComponent<AudioSource> ();
	}
		
	public void LevelSounds (string song)
	{
		if (song.Equals ("Volcan")) 
		{
			Audio.clip = Volcanica;
			Audio.Play ();
		}

		if (song.Equals ("Cenote")) 
		{
			Audio.clip = Cenote;
			Audio.Play ();
		}

		if (song.Equals ("Fabrica")) 
		{
			Audio.clip = Fabrica;
			Audio.Play ();
		}

		if (song.Equals ("Pantano")) 
		{
			Audio.clip = Pantano;
			Audio.Play ();
		}

		if (song.Equals ("Magica")) 
		{
			Audio.clip = Magica;
			Audio.Play ();
		}
	}
}
