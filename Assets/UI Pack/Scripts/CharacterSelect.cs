﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Spine.Unity;

public class CharacterSelect : MonoBehaviour {

	private GameObject[] playerList;
	private int index;

	// Use this for initialization
	void Start () {

		index = PlayerPrefs.GetInt ("playerSelected");

		playerList = new GameObject[transform.childCount];

		// llena el arreglo con los personajes jugables
		for (int i = 0; i < transform.childCount; i++) 

			playerList [i] = transform.GetChild (i).gameObject;

			//activa el primer personaje jugable en el indice de la lista
			foreach (GameObject go in playerList) 
				go.SetActive (false);

			if (playerList [index])
				playerList [index].SetActive (true);
	}

	public void SelectCharacterButton ()
	{
		playerList[index].SetActive (false);

		index--;
		if(index < 0)
			index = playerList.Length - 1;
		
		playerList [index].SetActive (true);
	}

	public void PlayGameButton ()
	{
		PlayerPrefs.SetInt ("playerSelected", index);
		PlayerPrefs.SetString ("nameskin", playerList [index].name);
		SceneManager.LoadScene ("NewLevel1");
	}

	void Upadate ()
	{
		
	}

}
