﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Creditos : MonoBehaviour {

	Text Credit;

	// Use this for initialization
	void Start () 
	{
		Credit = GetComponent<Text> ();
	}

	// Update is called once per frame
	void Update () 
	{
		if (Screen.orientation == ScreenOrientation.LandscapeLeft ||
			Screen.orientation == ScreenOrientation.LandscapeRight) {

			Credit.fontSize = 20;
		}
		else 
		{
			Credit.fontSize = 30;
		}
	}
}
