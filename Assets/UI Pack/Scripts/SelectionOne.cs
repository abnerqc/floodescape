﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectionOne : MonoBehaviour {

	private GameObject[] playerListOne;
	private int indexOne;
	void Start ()
	{
		CharacterOne ();
	}

	public void CharacterOne ()
	{
		indexOne = PlayerPrefs.GetInt ("playerOne");

		playerListOne = new GameObject[transform.childCount];

		// llena el arreglo con los personajes jugables
		for (int i = 0; i < transform.childCount; i++) 

			playerListOne [i] = transform.GetChild (i).gameObject;

		//activa el primer personaje jugable en el indice de la lista
		foreach (GameObject go in playerListOne) 
			go.SetActive (false);

		if (playerListOne [indexOne])
			playerListOne [indexOne].SetActive (true);
	}

	public void SelectCharacterOneButton ()
	{
		playerListOne[indexOne].SetActive (false);

		indexOne--;
		if(indexOne < 0)
			indexOne = playerListOne.Length - 1;

		playerListOne [indexOne].SetActive (true);
	}


	public void PlayGameButton ()
	{
		PlayerPrefs.SetInt ("playerOne", indexOne);
		SceneManager.LoadScene ("NewLevel1");
	}
		

}

