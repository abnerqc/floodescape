﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class CharacterSkin : MonoBehaviour {

	private GameObject[] playerList;
	private int index;

	// Use this for initialization
	void Start () 
	{
		index = PlayerPrefs.GetInt ("playerSelected");

		GetComponent<SkeletonAnimation> ().skeleton.SetSkin (PlayerPrefs.GetString ("nameskin"));

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
