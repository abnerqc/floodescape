﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelect : MonoBehaviour {

	private GameObject[] playerList;
	private int indexlevel;

	// Use this for initialization
	void Start () {

		indexlevel = PlayerPrefs.GetInt ("levelSelected");

		playerList = new GameObject[transform.childCount];

		// llena el arreglo con los personajes jugables
		for (int i = 0; i < transform.childCount; i++) 

			playerList [i] = transform.GetChild (i).gameObject;

		//activa el primer personaje jugable en el indice de la lista
		foreach (GameObject go in playerList) 
			go.SetActive (false);

		if (playerList [indexlevel])
			playerList [indexlevel].SetActive (true);
	}

	public void ActiveLeftButton ()
	{
		playerList[indexlevel].SetActive (false);

		indexlevel--;
		if(indexlevel < 0)
			indexlevel = playerList.Length - 1;

		playerList [indexlevel].SetActive (true);
	}

	public void ActiveRightButton ()
	{
		playerList [indexlevel].SetActive (false);

		indexlevel++;
		if (indexlevel == playerList.Length)
			indexlevel = 0;

		playerList [indexlevel].SetActive (true);
	}

	public void LevelGameButton ()
	{
		PlayerPrefs.SetInt ("levelSelected", indexlevel);
	}

	// Update is called once per frame

	void Update () {

	}
}
