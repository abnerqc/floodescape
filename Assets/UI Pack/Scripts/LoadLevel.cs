﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadLevel : MonoBehaviour {

	public string LoadLevelName = "Menu";

	public void LoadLevelFunction()
	{
		SceneManager.LoadScene  (LoadLevelName);
	}
}
