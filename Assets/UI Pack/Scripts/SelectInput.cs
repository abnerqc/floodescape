﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class SelectInput : MonoBehaviour {

	public EventSystem eventSystem;
	public GameObject selectObject;

	private bool buttonSelected;

	void Start ()
	{
	
	}

	void Update ()
	{
		if (Input.GetAxisRaw ("Vertical") != 0 && buttonSelected == false)
			{
			eventSystem.SetSelectedGameObject (selectObject);
			buttonSelected = true;
			}	
	}

	private void onDisable ()
	{
		buttonSelected = false;
	}

}
