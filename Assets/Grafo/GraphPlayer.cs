﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Spine.Unity;

public class GraphPlayer : MonoBehaviour {


	/// <summary>
	/// Animaciones
	/// </summary>
	public Animator myAnim;
	public FlexFSM fsm;
	//public SkeletonAnimation;

	public string animationName;
	public float speedanim;

	[Tooltip("Graph that is going to be walked by the player.")]
	public Grafo grafo;

	[Tooltip("Speed at which the player moves from one Waypoint to another.")]
	public float speed;

	[Tooltip("Energy to be used in player's movement")]
	public float stamina;

	public Text staminaText;

	[Tooltip("Used to track if player is active (if it's its turn)")]
	public bool Awake = true;

	/// <summary>
	/// Amount of stamina regained each turn by the player.
	/// </summary>
	public float staminaRegain = 1.2f;

	/// <summary>
	/// The max stamina that a player can have.
	/// </summary>
	float maxStamina = 10f;

	float journeyLength;
	float startTime;

	/// <summary>
	/// Used to track if the player is moving
	/// </summary>
	public bool moving;

	public bool falling;

	/// <summary>
	/// The winning text.
	/// </summary>
	public Text winText;

	public Text loseText;

	public GameObject water;

	/// <summary>
	/// The current item.
	/// </summary>
	Item currentItem;

	/// <summary>
	/// Next waypoint where player will go
	/// </summary>
	Waypoint nextWaypoint;

	/// <summary>
	/// Players current position in the path.
	/// </summary>
	Waypoint actualWaypoint;

	/// <summary>
	/// The spaces to move each turn
	/// </summary>
	public int spacesToMove = 0;

	/// <summary>
	/// Used to track if player has chosen the path when it bifurcates
	/// </summary>
	bool hasChosen = false;

	Directions direction;
	GraphPlayer targetPlayer;

	public GameAdmin gameAdmin;

	/// <summary>
	/// The canvas used to select a target player
	/// </summary>
	public Canvas TargetCanvas;

	/// <summary>
	/// Canvas containig the win text and UI elements
	/// </summary>
	public Canvas WinCanvas;

	/// <summary>
	/// Canvas containing object options and info
	/// </summary>
	public Canvas ObjectCanvas;

	// Use this for initialization
	void Start () {
		
		//myAnim = this.gameObject.GetComponent<Animator> ();
		//myfsm = myFlex.fsm;
		//flexAnim = this.gameObject.GetComponent<FlexState> ();
		//myfsm.ChangeState (CharacterStates.StateID.VerticalIdle);

		startTime = Time.time;
		actualWaypoint = grafo.root;
		direction = Directions.None;
		setAnimation ("presentation", false);

		TargetCanvas.gameObject.SetActive (false);
		WinCanvas.gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if(Awake){

			if (moving) {



				//distance covered to this point
				float distCovered = (Time.time - startTime) * speed;
				//distance left to next waypoint
				float fracJourney = distCovered / journeyLength;


				//Move to next waypoint
				transform.position = Vector3.MoveTowards (transform.position, nextWaypoint.transform.position, Time.deltaTime * speed);

				if (Vector3.Distance (transform.position, nextWaypoint.transform.position) <= 0.002f) {
					if (spacesToMove <= 0) {
						moving = false;

						Debug.Log ("Ya llegué.");

						//Aquí se detiene, aquí cambiar a Idle
						setAnimation ("V_idle", true);

						stamina += staminaRegain;

						if (stamina > maxStamina)
							stamina = maxStamina;

//						water.SendMessage ("Rise");
						if (currentItem != null) {
						
							if (currentItem.activeOnSelf ()) {
								Debug.Log ("Activate item on self");
								currentItem.Activate (this);
								staminaText.text = ("Stamina: " + stamina.ToString ());

								//Aquí activa item

								currentItem = null;

								if (spacesToMove <= 0)
									//Aquí temina el turno

									Sleep ();
							} else {
								Debug.Log ("Activate item on other player");
								hasChosen = false;
								StartCoroutine (ChosingTarget ());
							}
//						Destroy (currentItem);
						} else {
							staminaText.text = ("Stamina: " + stamina.ToString ());
//							gameAdmin.gameObject.SendMessage ("EndTurn", this);

							//Aquí termina el turno

							Sleep ();

						}




					} else if (actualWaypoint.bifurcates) {
						Debug.Log ("Actual: " + actualWaypoint.name);
						Debug.Log ("Spaces to move: " + spacesToMove.ToString ());
						Debug.Log ("Bifurcates");

						moving = false;

						//Aquí se detiene a decidir camino

						StartCoroutine (Chosing ());

						journeyLength = Vector3.Distance (transform.position, nextWaypoint.transform.position);
					} else {
						nextWaypoint = actualWaypoint.next;
						journeyLength = Vector3.Distance (transform.position, nextWaypoint.transform.position);

						spacesToMove--; 

					}
				}
			}
		}

		if (falling) {
			float distCovered = (Time.time - startTime) * speed;
			float fracJourney = distCovered / journeyLength;

			//Aquí cae

			transform.position = Vector3.MoveTowards (transform.position, nextWaypoint.transform.position, Time.deltaTime * speed);

			if (Vector3.Distance(transform.position, nextWaypoint.transform.position) <= 0.002f ) {
				if (spacesToMove <= 0) {
					falling = false;

				} 

				else {
					nextWaypoint = actualWaypoint.previous;
					journeyLength = Vector3.Distance (transform.position, nextWaypoint.transform.position);

					spacesToMove--; 

				}
			}
		}
	}


	/// <summary>
	/// Move the specified spaces in the path.
	/// </summary>
	/// <param name="spaces">Spaces to move.</param>
	public void Move(int spaces){
		if (Awake) {
			if (actualWaypoint.bifurcates)
				StartCoroutine (Chosing ());
			else
				nextWaypoint = actualWaypoint.next;
		
			spacesToMove = spaces;

			//Aquí comienza a moverse 
			setAnimation ("V_move", true);
			if (spacesToMove > 0)
				moving = true;

			spacesToMove--;
		}
	}

	public void Fall(int spaces){
		nextWaypoint = actualWaypoint.previous;

		spacesToMove = spaces;

		if (spacesToMove > 0)
			falling = true;

		spacesToMove--;
	}

	IEnumerator Chosing(){
		while (!hasChosen) {
			yield return null;
		}

		if (direction == Directions.Left && hasChosen) {
			nextWaypoint = actualWaypoint.leftWaypoint;
			direction = Directions.None;
		}

		if (direction == Directions.Right && hasChosen) {
			nextWaypoint = actualWaypoint.rightWaypoint;
			direction = Directions.None;
		}

		hasChosen = false;
		spacesToMove--;

		moving = true;
	}

	IEnumerator ChosingTarget(){

//		gameAdmin.ChoosePlayerCamera ();

		TargetCanvas.gameObject.SetActive (true);
		while (!hasChosen) {
			yield return null;
		}
			
		Debug.Log ("Chosen target " + targetPlayer.name);

		hasChosen = false;
		currentItem.Activate (targetPlayer);
		targetPlayer = null;
		currentItem = null;

		staminaText.text = ("Stamina: " + stamina.ToString ());


		TargetCanvas.gameObject.SetActive (false);
		gameAdmin.gameObject.SendMessage ("EndTurn", this);
		Sleep ();

	}

	IEnumerator ChoosingItem(){
		ObjectCanvas.gameObject.SetActive (true);

		while (!hasChosen) {
			yield return null;
		}

		Debug.Log ("ItemPickedUp");

		//Pick up item from UI or admin


	}

	public void Choose(Directions dir){
		direction = dir;
		Debug.Log ("Direction received: " + direction.ToString ());
		hasChosen = true;
	}

	void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "Waypoint") {
			actualWaypoint = col.gameObject.GetComponent<Waypoint> ();
			if (actualWaypoint.lastWaypoint)
				Win ();
		}
		if (col.tag == "Water") {
			moving = false;
			loseText.gameObject.SetActive (true);
		}

		if (col.tag == "Item") {
//			currentItem = col.GetComponent<Item>();
//			Debug.Log ("Item picked up");
			col.gameObject.SetActive (false);
//			Destroy (col.gameObject);
		}
	}

	void ChooseTargetPlayer(GraphPlayer target){
		targetPlayer = target;
		hasChosen = true;
	}

//	void OnMouseDown(){
//		gameAdmin.activePlayer.targetPlayer = this;
//		gameAdmin.activePlayer.hasChosen = true;	
//	}

	void Win(){
		moving = false;
		WinCanvas.gameObject.SetActive (true);
//		winText.gameObject.SetActive (true);
	}

	/// <summary>
	/// Wakes up the player so it can execute its turn.
	/// </summary>
	public void WakeUp(){
		Awake = true;
		setAnimation ("V_idle", true);

		staminaText.text = ("Stamina: " + stamina.ToString ());
//		if (stamina < 1)
//			Sleep ();
			
	}

	/// <summary>
	/// Sleep this player at the end of the turn.
	/// </summary>
	public void Sleep(){
		Awake = false;
		setAnimation ("V_idle", true);
		gameAdmin.gameObject.SendMessage ("EndTurn", this);
	}




	public void setAnimation(string animationName, bool loop) {

		//SkeletonAnimation anim = this.gameObject.GetComponent<SkeletonAnimation> ();
		SkeletonAnimation anim = this.gameObject.GetComponentInChildren<SkeletonAnimation> ();
		anim.state.SetAnimation (0, animationName, loop).timeScale = 2f;
	}


}