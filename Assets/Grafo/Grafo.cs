﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Grafo : MonoBehaviour {

	[Tooltip("Graphs root waypoint.")]
	public Waypoint root;

//	[HideInInspector]
	public List<Waypoint> graph;

	// Use this for initialization
	void Start () {
//		InitGrapgh (root);

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void AddVertex(Waypoint vertex){
		if(!graph.Contains(vertex))
			graph.Add (vertex);
	}

	void RemoveVertex(Waypoint vertex){
		if(graph.Contains(vertex))
			graph.Remove (vertex);
	}

	/*void InitGrapgh(Waypoint r){
		if (graph == null) {
			graph = new List<Waypoint> ();
			Debug.Log ("New Graph");
		}

		if (r != null && !graph.Contains (r)) {
			graph.Add (r);
			Debug.Log ("New root");
		}

		//		if (root == null) {
		//			root = GameObject.Find ("Raiz").GetComponent<Waypoint> ();

		//		} else {
		Waypoint[] connections = r.connections;
		//			while (connections != null) {
		//				AddVertex (next);
		//				next = next.connections;
		//			}

		if (connections.Length > 0) {
			foreach (Waypoint w in connections) {
				if(w.weight > r.weight)
					InitGrapgh (w);
			}
		} else
			return;
		//		}
	}
*/
	void InitGrapgh(Waypoint r){
		if (graph == null) {
			graph = new List<Waypoint> ();
			Debug.Log ("New Graph");
		}

		if (r != null && !graph.Contains (r)) {
			AddVertex (r);
			Debug.Log ("New root");
		}
			
		if (!r.bifurcates) {
			if (r.next != null)
				InitGrapgh (r.next);
			else
				return;

		} else {
			if (r.leftWaypoint != null)
				InitGrapgh (r.leftWaypoint);

			if(r.rightWaypoint != null) 
				InitGrapgh (r.rightWaypoint);
			
			return;
		}
	}
}
