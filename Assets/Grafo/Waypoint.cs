﻿using UnityEngine;
using System.Collections;

public class Waypoint : MonoBehaviour {

//	[Tooltip("Siguiente waypoint en el flujo")]
//	public Waypoint[] connections;
	[Tooltip("Anterior waypoint en el flujo")]
	public Waypoint previous;
	[Tooltip("Si es el primer Waypoint del camino")]
	public bool root;

	public GraphPlayer player;

	[Tooltip("Used to track winning condition")]
	public bool lastWaypoint = false;
	/// <summary>
	/// Used to know if waypoint bifurcates the path.
	/// </summary>
//	[HideInInspector]
	public bool bifurcates = false;

	public Waypoint leftWaypoint;

	public Waypoint rightWaypoint;

	public Waypoint next;

	public Waypoint(){
//		connections = null;
		previous = null;
		root = false;
	}

	// Use this for initialization
	void Start () {
//		if (connections.Length > 1)
//			bifurcates = true;
//		else
//			bifurcates = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//Dibuja la representación gráfica del waypoint en el editor de Unity
	void OnDrawGizmos(){
		Gizmos.color = Color.yellow;

		if (root)
			Gizmos.color = Color.blue;

		Gizmos.DrawSphere (transform.position, 0.5f);

		if (next != null) {
			Gizmos.color = Color.blue;
			Gizmos.DrawLine (transform.position, next.transform.position);
		}

		if (leftWaypoint != null) {
			Gizmos.color = Color.blue;
			Gizmos.DrawLine (transform.position, leftWaypoint.transform.position);
		}

		if (rightWaypoint != null) {
			Gizmos.color = Color.blue;
			Gizmos.DrawLine (transform.position, rightWaypoint.transform.position);
		}
	}

	void OnDrawGizmosSelected(){
		Gizmos.color = Color.red;

		Gizmos.DrawSphere (transform.position, 0.5f);

//		if(connections != null){
//			Gizmos.color = Color.blue;
//			foreach(Waypoint w in connections)
//				Gizmos.DrawLine (transform.position, w.transform.position);
//		}
		if (next != null) {
			Gizmos.color = Color.blue;
			Gizmos.DrawLine (transform.position, next.transform.position);
		}

		if (leftWaypoint != null) {
			Gizmos.color = Color.blue;
			Gizmos.DrawLine (transform.position, leftWaypoint.transform.position);
		}

		if (rightWaypoint != null) {
			Gizmos.color = Color.blue;
			Gizmos.DrawLine (transform.position, rightWaypoint.transform.position);
		}
	}

	void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "Player") {
			player = col.GetComponent<GraphPlayer> ();
		}
	}
}