﻿using UnityEngine;
using System.Collections;

public enum Directions { Right, Left, None }
public class DirectionButtonScript : MonoBehaviour {

	[Tooltip("Where the player will go if they press this button")]
	public Directions direction;

	[Tooltip("Waypoint where the path bifurcates")]
	public Waypoint bifurcation;

	 
	void OnMouseDown(){
		Debug.Log ("Direction sent: " + direction.ToString ());
		bifurcation.player.SendMessage ("Choose", direction);
	}
}
